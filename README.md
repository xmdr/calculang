# Calculang

Calculang is a little infix calculator language designed to run in the terminal.

# Building

    gcc main.c -lm -o calc

# Usage

    calc 'x=43 y=12 x*y+pi*e /. this is a comment ./ 1^2^3!'

Use normal infix notation. Calculang correctly handles operator precedence and associativity.

The evaluation of each consecutive expression is printed.

`pi` and `e` predefined to their expected values.

Running without an argument makes it run a test program.

# Operators

The best way to see the operators available is to look into `parser.c`. It starts out with an operator table recording the precedence/priority of each operator, as well as if it's prefix unary (nud, an operator with *no* lhs hand side) or postfix/infix (led, an operator *with* a lhs), and if it is a "led", wether it should also have a right-hand-side (making it infix binary) or no right side (making it postfix unary).

# Workings

The calculang interpreter is a 2 pass one: first it parses (with a precedence climber/Pratt parser, tokenization is inlined into the parsing) the program into an AST (abstract syntax tree). Then it evaluates that tree to a `double` using recursion.
