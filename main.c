#include <math.h>
#include "parser.c"

////////////////////////
// SYMBOL TABLE

static struct {
	const char *key;
	double      value;
	unsigned    keylen;
	unsigned    hash;
} table[256];

static inline int lookup(Lexeme lxm, double **dest){
	const char *key  = lxm.sValue;
	unsigned    len  = lxm.len;
	unsigned    hash = lxm.sHash;
	
	for (int offs=0; offs<256; offs++){
		int i = (hash+offs)&0xff;
		if ( hash == table[i].hash   &&
			len  == table[i].keylen &&
			memcmp(key, table[i].key, len) == 0
		){
			*dest = &table[i].value;
			return 1;
		}
	}
	return 0;
}

static inline int insert(Lexeme lxm, double val){
	const char* key  = lxm.sValue;
	unsigned    len  = lxm.len;
	unsigned    hash = lxm.sHash;

	for (int offs=0; offs<256; offs++){
		int i = (hash+offs)&0xff;
		if (!table[i].key){
			table[i].key = key;
			table[i].value = val;
			table[i].keylen = len;
			table[i].hash = hash;
			return 1;
		}
	}
	puts("RUNTIME ERROR: attempted to define more than 256 variables");
	exit(1);   
}

//////////////////////////////
// RUNTIME

static AST *simplify(AST*);
static AST *deriv(AST*);
static double eval(AST*);

int isZero(double x){
	return fabs(x) < 0.00001;
}

AST *num2ast(double n){
	return AST_new(
		(Lexeme){.tok=t_num, .dValue=n},
		NULL, NULL
	);
}

static inline AST *simplify(AST *a){
	if (a == NULL) return a;
	AST *l = a->l;
	AST *r = a->r;
	// constant eval
	if (l && l->lxm.tok == t_num && r && r->lxm.tok == t_num){
		a->lxm.dValue = eval(a);
		a->lxm.tok = t_num;
		a->l = a->r = NULL;
		return a;
	}
	switch(a->lxm.tok){
		case t_num:
		case t_var:
			return a;
		case '*':
			if (l->lxm.tok == t_num){
				// 0*x
				if (isZero(l->lxm.dValue)){
					a->lxm.dValue = 0;
					a->lxm.tok = t_num;
					a->l = a->r = NULL;
					return a;
				}
				// 1*x
				if (isZero(l->lxm.dValue-1)){
					return simplify(r);
				}
			}
			if (r->lxm.tok == t_num){
				// x*0
				if (isZero(r->lxm.dValue)){
					a->lxm.dValue = 0;
					a->lxm.tok = t_num;
					a->l = a->r = NULL;
					return a;
				}
				// x*1
				if (isZero(r->lxm.dValue-1)){
					return simplify(l);
				}
			}
		default:
			l = simplify(l);
			r = simplify(r);
			a->l = l;
			a->r = r;

			if (l && l->lxm.tok == t_num && r && r->lxm.tok == t_num){
				a->lxm.dValue = eval(a);
				a->lxm.tok = t_num;
				a->l = a->r = NULL;
				return a;
			}

			return a;
	}
}

// take an AST, return a new AST which is the derivative
static inline AST *deriv(AST *a){
	AST *f = a->r;
	AST *g = a->r;
	switch(a->lxm.tok){
		// x' -> 1
		case t_var:
		return AST_new((Lexeme){t_num,.dValue=1},NULL,NULL);
		
		// c' -> 0
		case t_num:
		return AST_new((Lexeme){t_num,.dValue=0},NULL,NULL);
		
		// (f+g)' -> f' + g'
		case '+':
		return oper('+', deriv(f), deriv(g));

		case '-':
		return oper('-', deriv(f), deriv(g));

		// (f/g)' -> (f'g - f*g') / (g*g)
		case '/':
		return oper('/',
			oper('-',
				oper('*', deriv(f), g),
				oper('*', f, deriv(g))
			),
			oper('*', g, g)
		);

		// f*g -> f'*g + f*g'
		case '*':
		return oper('+',
			oper('*', deriv(f), g),
			oper('*', f, deriv(f))
		);

		case '^':
		// (e^x)' -> x'*e^x
		if (f->lxm.tok == t_var && f->lxm.len == 1 && *f->lxm.sValue == 'e'){
			return oper('*', deriv(g), a);
		}
		// (x^n)' -> n*x^(n-1)
		else if (g->lxm.tok == t_var || g->lxm.tok == t_num){
			AST *one = AST_new((Lexeme){t_num, .dValue=1},NULL,NULL);
			return oper('*', g, oper('^', f, oper('-', g, one)));
		}

		default:
		puts("cant derive this:");
		AST_dump(a,0);
		return a;
	}
}

static inline double eval(AST *a){
	double *tmp = NULL;
	AST *l = a->l;
	AST *r = a->r;
	switch(a->lxm.tok){
		case t_num: return a->lxm.dValue;
		case '>': return eval(l) > eval(r);
		case '<': return eval(l) < eval(r);
		case '-': return (l?eval(l):0) - eval(r);
		case '+': return eval(l) + eval(r);
		case '/': return eval(l) / eval(r);
		case '*': return eval(l) * eval(r);
		case '^': return powl(eval(l), eval(r));
		case '%': return fmodl(eval(l), eval(r));
		case '!': return tgamma(eval(l)+1);
		case '?': AST_dump(a,0); return eval(l);
		case '#': return (double)l->lxm.sHash;
		case '$': {
			printf("derivative of ");AST_dump(l,0);
			AST *d = deriv(l);
			printf("is: ");AST_dump(d,0);
			AST *s = simplify(d);
			printf("simplified: ");AST_dump(s,0);
			return eval(d);
		}
		case '@': {
			printf("simplified: ");
			AST_dump(simplify(l),0);
			return eval(l);
		}
		case '=':
		if (!lookup(l->lxm, &tmp)){
			// declare/assign
			double res = eval(r);
			insert(l->lxm, res);
			return res;
		} else {
			// reassign (tmp aliases table entry)
			*tmp = eval(r);
			return *tmp;
		}
		case t_var: {
			if (!lookup(a->lxm, &tmp)){
				printf("unkown variable: ");
				Lexeme_dump(a->lxm);
				exit(1);
			}
			return *tmp;
		}

		default:
		puts("unknown token: ");
		Lexeme_dump(a->lxm);
		AST_dump(a,0);
		exit(1);
	}
}

int main(int argc, char *argv[argc]){
	head = "e = 2.718281828 pi = 3.141592654";
	lex();
	eval(parse_expr(0));
	eval(parse_expr(0));
	
	if (argc==1){
		head = "x=0 ;x=x+3\nx=1/.as./x=(x+3)";
		lex();
		while (curr.tok){
			printf(">>> %f\n", eval(parse_expr(0)));
		}
		return 0;
	}
	for (int i=1; i<argc; i++){
		head = argv[i];
		lex();
		while (curr.tok){
			printf(">>> %f\n", eval(parse_expr(0)));
		}
	}
	return 0;
}
