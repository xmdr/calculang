#include "tokenizer.c"

////////////////////////
// OPERATOR TABLE

// nud: null denotated, a token without left operand
// led: left denotated, a token with a  left operand
// terminal tokens    are nud without right operand, such as 1 or x
// prefix   operators are nud with a  right operand, such as -
// postfix  operators are led without right operand, such as !
// infix    operators are led with a  right operand, such as +

// priority > 0: implies led exists
// priority % 2: right associative (a^b)
// has_nud == 1: null denotated exists
// led_rhs == 1: the led version has a rhs
// both led/nud can exist in one operator:
// `-` operator (nud `-a` and led `a-b`)
#define ASSOC_MASK 0b111110
static const struct {
	unsigned char priority:6; 
	unsigned char has_nud :1;
	unsigned char led_rhs :1;
} ops[255] = {
	[t_eof]={ 0,0,0},
	[t_var]={ 0,0,0},
	[t_num]={ 0,0,0},
	[ '(' ]={ 0,1,0},
	[ '?' ]={ 2,0,0}, // diagnostic
	[ '=' ]={ 3,0,1}, // assign
	[ '$' ]={ 4,0,0}, // derivative
	[ '@' ]={ 4,0,0}, // simplify
	[ '>' ]={ 4,0,1},
	[ '<' ]={ 4,0,1},
	[ '+' ]={ 6,0,1}, // addition
	[ '-' ]={ 6,1,1}, // subtraction and negation
	[ '*' ]={ 8,0,1}, // multiplication
	[ '/' ]={ 8,0,1}, // division
	[ '%' ]={ 8,0,1}, // modulo
	[ '^' ]={ 9,0,1}, // exponent a^b
	[ '!' ]={10,0,0}, // factorial gamma(n+1)
	[ '#' ]={10,0,0}, // hash
	// all other entries are all 0
};

////////////////////////
// ABSTRACT SYNTAX TREE

typedef struct AST AST;
struct AST {
	Lexeme lxm;
	AST *l, *r;
};

static inline void AST_dump(AST *a, int depth){
	if (a == NULL) return;
	for(int i=0; i<depth; i++){
		printf("    ");
	}
	Lexeme_dump(a->lxm);
	AST_dump(a->l, depth+1);
	AST_dump(a->r, depth+1);
}

static inline AST *AST_new(Lexeme x, AST *l, AST *r){
	#define AST_REGION_SIZE ((1024*1024)/sizeof(AST))
	static AST AST_region[AST_REGION_SIZE];
	static size_t AST_count = 0;
	if (AST_count >= AST_REGION_SIZE){
		puts("INTERNAL ERROR: AST memory region depleted");
		exit(1);
	}
	AST_region[AST_count] = (AST){x,l,r};
	return &AST_region[AST_count++];
}

static inline AST *oper(char op, AST *l, AST *r){
	return AST_new((Lexeme){op}, l, r);
}

////////////////////////
// PRATT PARSER

static AST *parse_expr(int);
static AST *parse_left(Lexeme);
static AST *parse_right(Lexeme, AST*);

static inline AST *parse_expr(int priority){
	Lexeme tmp = lex();
	AST   *lhs = parse_left(tmp);
	// op.priority < priority means the parent caller will handle that operator
	while (lhs && ops[curr.tok].priority > priority){
		tmp = lex();
		lhs = parse_right(tmp, lhs);
	}
	return lhs;
}

static inline AST *parse_left(Lexeme lxm){
	Token t = lxm.tok;
	if (t == t_eof){
		puts("SYNTAX ERROR: unfinished expression, program source code ends prematurely");
		exit(1);
	}
	if (t == t_var || t == t_num){
		return AST_new(lxm, NULL, NULL);
	}
	if (!ops[t].has_nud){
		printf("SYNTAX ERROR: token is not a prefix operator/operand: ");
		Lexeme_dump(lxm);
		exit(1);
	}
	AST *rhs = parse_expr(0);
	if (t == '('){
		if (curr.tok != ')'){
			printf("SYNTAX ERROR: expected ), got ");
			Lexeme_dump(curr);
			exit(1);
		}
		lex();
		return rhs;
	}
	return AST_new(lxm, NULL, rhs);
}

static inline AST *parse_right(Lexeme lxm, AST *lhs){
	Token t = lxm.tok;
	if (ops[t].priority == 0){
		printf("SYNTAX ERROR: token is not a postfix/infix operator: ");
		Lexeme_dump(lxm);
		exit(1);
	}
	AST *rhs = ops[t].led_rhs
		? parse_expr(ops[t].priority & ASSOC_MASK)
		: NULL;
	return AST_new(lxm, lhs, rhs);
}
