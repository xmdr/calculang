#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

typedef enum {t_eof, t_var, t_num, t_sym} Token;
typedef struct Lexeme Lexeme;

char *head = NULL;
struct Lexeme {
	Token tok;
	unsigned len;
	unsigned line;
	unsigned col;
	union {
		struct {
			const char* sValue;
			unsigned sHash;
		};
		double dValue;
	};
} curr = {.line=1, .col=1};

static inline void Lexeme_dump(Lexeme lxm){
	switch(lxm.tok){
		case t_var:
		printf("var %.*s\n", lxm.len, lxm.sValue);
		break;

		case t_num:
		printf("num %f\n", lxm.dValue);
		break;

		default:
		printf("opr %c\n", lxm.tok);
		break;
	}
}
static inline void Lexeme_debug(Lexeme lxm){
	printf("%08x %08x %p %08x ", lxm.tok, lxm.len, lxm.sValue, lxm.sHash);
	Lexeme_dump(lxm);
}

static inline Lexeme lex(){
	Lexeme old = curr;
	char *end  = NULL;
	curr.len   = 0;
	top: switch (*(unsigned char*)head){
		case 0:
		curr.tok = t_eof;
		return old;

		case 1  ...  8:
		case 11 ... 12:
		case 14 ... 31:
		case 127:
		printf("LEXICAL ERROR: invalid char 0x%x after %.5s\n", *head, head-1);
		exit(1);

		case '\n':
		curr.line++;
		curr.col = 0;
		case '\t':
		case '\r':
		case ' ':
		head++;
		curr.col++;
		goto top;

		case '0'...'9':
		curr.dValue = strtod(head, &end);
		curr.len    = (int)(end - head);
		curr.col   += curr.len;
		curr.tok    = t_num;
		head        = end;
		return old;
		
		case '_':
		case 'a' ... 'z':
		case 'A' ... 'Z':
		case 128 ... 255: // unicode shit
		curr.sValue = head;
		// Calculate FNV-1a hash as we go
		curr.sHash  = 2166136261; // FNV offset basis
		do {
			curr.sHash ^= *head;
			curr.sHash *= 16777619; // FNV prime
			head++; curr.len++;
		} while (isalnum(*head) || *head>127 || *head=='_');
		curr.col += curr.len;
		curr.tok  = t_var;
		return old;

		// line comment
		case ';': {
			char *tmp = strchr(head, '\n');
			if (tmp){
				curr.len += tmp-head;
				head = tmp;
				goto top;
			} else {
				puts("LEXICAL ERROR: unterminated ;comment");
				exit(1);
			}
		}

		// long comment
		case '/':
		if (head[1] == '.'){
			curr.len++; head++; // consume /
			curr.len++; head++; // consume .
			char *tmp = strstr(head, "./");
			if (tmp){
				curr.len += tmp+2-head;
				head = tmp+2;
			}
			goto top;
		}
		// else fall through

		default:
		curr.len = 1;
		curr.tok = *head++;
		return old;
	}
}
